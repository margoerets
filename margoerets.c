#include <gtk/gtk.h>

struct image_coord {
	struct image_coord *next;
	double x;
	double y;
};

struct perspective_info {
	GtkNotebook *notebook;
	int page;
	GdkPixbuf *original;
	GdkPixbuf *resized;
	struct image_coord *path;
	struct image_coord *path_end;
	int path_length;
};

void margoerets_open_output(GtkFileChooser *chooser, gpointer user_data)
{
	char *filename = gtk_file_chooser_get_filename(chooser);
	printf("Open %s\n", filename);
	g_free(filename);

	gtk_widget_hide(GTK_WIDGET(chooser));
}

void margoerets_expose_perspective(GtkWidget *widget,
				GdkEventExpose *event,
				gpointer user_data)
{
	GtkDrawingArea *perspective_drawingarea = GTK_DRAWING_AREA(widget);
	struct perspective_info *perspective = user_data;
	struct image_coord *node;
	cairo_t *cr;

	printf("exposed\n");

	cr = gdk_cairo_create(perspective_drawingarea->widget.window);

	gdk_cairo_set_source_pixbuf(cr, perspective->resized, 0, 0);
	cairo_paint(cr);

	if (perspective->path) {
		cairo_set_source_rgb(cr, 1, 1, 0);
		cairo_move_to(cr, perspective->path->x, perspective->path->y);
		for (node = perspective->path->next; node; node = node->next) {
			printf("Line to %g,%g\n",
			       node->x, node->y);
			cairo_line_to(cr, node->x, node->y);
		}
		cairo_stroke(cr);
	}

	cairo_destroy(cr);
}

void margoerets_configure_perspective(GtkWidget *widget,
				   GdkEventConfigure *event,
				   gpointer user_data)
{
	struct perspective_info *perspective = user_data;

	printf("configure %d %d,%d+%d,%d send_event=%d on window %p\n",
	       event->type, event->x, event->y, event->width, event->height, event->send_event, event->window);

	if (perspective->resized) {
		g_object_unref(G_OBJECT(perspective->resized));
	}
	perspective->resized = gdk_pixbuf_scale_simple(perspective->original, event->width, event->height, GDK_INTERP_NEAREST);
}

void margoerets_advance_page(GtkNotebook *notebook,
			     int path_length)
{
	GList *notebook_perspectives;
	int i, n_pages;

	/* Advance to next page with a shorter path (by 1 node) after adding a node. */
	g_object_get(G_OBJECT(notebook), "user-data", &notebook_perspectives, NULL);
	for (i = 0; notebook_perspectives; i++) {
		struct perspective_info *perspective = notebook_perspectives->data;
		printf("Perspective: %d length = %d, looking for %d\n", i, perspective->path_length, path_length - 1);
		if (perspective->path_length == path_length - 1) {
			g_object_set(perspective->notebook,
				     "page", perspective->page,
				     NULL);
			return;
		}
		notebook_perspectives = notebook_perspectives->next;
	}
}

gboolean margoerets_button_press_perspective(GtkWidget *widget,
					     GdkEventButton *event,
					     gpointer user_data)
{
	GtkDrawingArea *perspective_drawingarea = GTK_DRAWING_AREA(widget);
	GdkRegion *visible_region;
	struct perspective_info *perspective = user_data;
	struct image_coord *node;

	printf("button %d %g,%g\n",
	       event->type, event->x, event->y);

	node = g_new(struct image_coord, 1);
	node->next = NULL;
	node->x = event->x;
	node->y = event->y;

	if (perspective->path_end) {
		perspective->path_end->next = node;
	}
	perspective->path_end = node;
	if (!perspective->path) {
		perspective->path = node;
	}
	perspective->path_length++;

	/* Just invalidate everything. */
	visible_region = gdk_drawable_get_visible_region(perspective_drawingarea->widget.window);
	gdk_window_invalidate_region(perspective_drawingarea->widget.window,
				     visible_region,
				     FALSE);
	gdk_region_destroy(visible_region);

	margoerets_advance_page(perspective->notebook, perspective->path_length);

	return TRUE;
}

void margoerets_load_perspectives_slist(GtkNotebook *notebook,
					GSList *perspectives)
{
	GSList *iter;

	for (iter = perspectives; iter; iter = iter->next) {
		char *path = g_file_get_path(G_FILE(iter->data));

		struct perspective_info *perspective = g_new(struct perspective_info, 1);

		GdkPixbuf *pixbuf = gdk_pixbuf_new_from_file(path, NULL);
		int width = gdk_pixbuf_get_width(pixbuf);
		int height = gdk_pixbuf_get_height(pixbuf);

		GList *notebook_perspectives;

		g_object_get(G_OBJECT(notebook), "user-data", &notebook_perspectives, NULL);
		notebook_perspectives = g_list_prepend(notebook_perspectives, perspective);
		g_object_set(G_OBJECT(notebook), "user-data", notebook_perspectives, NULL);

		GtkDrawingArea *perspective_drawingarea = GTK_DRAWING_AREA(gtk_drawing_area_new());

		printf("Perspective: %s -> pixbuf %p %dx%d drawing area %p for notebook %p\n",
		       path, pixbuf, width, height, perspective_drawingarea, notebook);

		perspective->notebook = notebook;
		perspective->original = pixbuf;
		perspective->resized = NULL;
		perspective->path = NULL;
		perspective->path_end = NULL;
		perspective->path_length = 0;

		g_signal_connect(G_OBJECT(perspective_drawingarea), "expose-event",
				 G_CALLBACK(&margoerets_expose_perspective), perspective);
		g_signal_connect(G_OBJECT(perspective_drawingarea), "configure-event",
				 G_CALLBACK(&margoerets_configure_perspective), perspective);
		g_signal_connect(G_OBJECT(perspective_drawingarea), "button-press-event",
				 G_CALLBACK(&margoerets_button_press_perspective), perspective);

		gtk_widget_set_events(GTK_WIDGET(perspective_drawingarea),
				      gtk_widget_get_events(GTK_WIDGET(perspective_drawingarea))
				      | GDK_BUTTON_PRESS_MASK);
		gtk_widget_set_size_request(GTK_WIDGET(perspective_drawingarea), width/4, height/4);
		gtk_widget_show(GTK_WIDGET(perspective_drawingarea));

		perspective->page = gtk_notebook_append_page(notebook, GTK_WIDGET(perspective_drawingarea), NULL);
		g_free(path);
	}
}

void margoerets_load_perspectives(GtkFileChooser *chooser, gpointer user_data)
{
	GSList *perspectives = gtk_file_chooser_get_files(chooser);
	GtkObject *margoerets;
	GtkBuilder *gtk_builder;
	GtkNotebook *notebook;

	/* Get hold of the GtkBuilder. */
	g_object_get(chooser, "user-data", &margoerets, NULL);
	g_object_get(margoerets, "user-data", &gtk_builder, NULL);

	notebook = GTK_NOTEBOOK(gtk_builder_get_object(gtk_builder, "perspectives_notebook"));
	gtk_widget_show(GTK_WIDGET(notebook));

	margoerets_load_perspectives_slist(notebook, perspectives);

	g_slist_free(perspectives);

	gtk_widget_hide(GTK_WIDGET(chooser));
}

void connect_signal(GtkBuilder *builder,
		    GObject *object,
		    const gchar *signal_name,
		    const gchar *handler_name,
		    GObject *connect_object,
		    GConnectFlags flags,
		    gpointer user_data)
{
	static struct {
		char const *name;
		GCallback fn;
	} handlers[] = {
		{ "gtk_main_quit", G_CALLBACK(&gtk_main_quit) },
		{ "gtk_widget_hide_on_delete", G_CALLBACK(&gtk_widget_hide_on_delete) },
		{ "gtk_widget_show", G_CALLBACK(&gtk_widget_show) },
		{ "margoerets_open_output", G_CALLBACK(&margoerets_open_output) },
		{ "margoerets_load_perspectives", G_CALLBACK(&margoerets_load_perspectives) },
		{ NULL, NULL }
	};
	int i;

	for (i = 0; handlers[i].name; i++) {
		if (!strcmp(handlers[i].name, handler_name)) {
			GCallback handler = handlers[i].fn;
			g_signal_connect_object(object, signal_name, handler,
						connect_object, flags);
			return;
		}
	}

	/* TODO: Connect to some error-spewing function? */
}

int main(int argc, char *argv[])
{
	GtkBuilder *gtk_builder;
	GtkWindow *margoerets_window;

	gtk_init(&argc, &argv);

	gtk_builder = gtk_builder_new();
	gtk_builder_add_from_file(gtk_builder, "margoerets.glade", NULL);
	gtk_builder_connect_signals_full(gtk_builder, &connect_signal, NULL);

	/* Give signal handlers a means of accessing gtk_builder. */
	g_object_set(gtk_builder_get_object(gtk_builder,
					    "margoerets_window"),
		     "user-data", gtk_builder,
		     NULL);

	/*
	 * Connect the file chooser dialog to its parent application window.
	 * In future, there might be multiple application windows, and this
	 * connection will have to depend on which window opens the dialog.
	 */
	g_object_set(gtk_builder_get_object(gtk_builder,
					    "perspectives_filechooserdialog"),
		     "user-data", gtk_builder_get_object(gtk_builder,
							 "margoerets_window"),
		     NULL);

	if (argc > 1) {
		GSList *perspectives = NULL;
		GtkNotebook *notebook;
		int i;

		for (i = argc; i-- > 1; ) {
			printf("Arg %d: %s\n", i, argv[i]);
			perspectives = g_slist_prepend(perspectives, g_file_new_for_commandline_arg(argv[i]));
		}

		notebook = GTK_NOTEBOOK(gtk_builder_get_object(gtk_builder, "perspectives_notebook"));
		margoerets_load_perspectives_slist(notebook, perspectives);
		g_slist_free(perspectives);
	}

	margoerets_window = GTK_WINDOW(gtk_builder_get_object(gtk_builder, "margoerets_window"));
	gtk_widget_show(GTK_WIDGET(margoerets_window));

	gtk_main();
}
